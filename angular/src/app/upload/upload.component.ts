import {Component, EventEmitter, Output} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss'],
  standalone: true
})
export class UploadComponent {
  selectedFile?: File;
  @Output() refreshBandList = new EventEmitter<string>();

  constructor(private http: HttpClient) {}

  onFileSelected(event: Event) {
    event.preventDefault();
    const input = event.target as HTMLInputElement;
    if (input && input.files && input.files.length) {
      this.selectedFile = input.files[0];
    }
  }

  uploadFile(event: Event, fileInput: HTMLInputElement) {
    event.preventDefault();
    if (this.selectedFile) {
      const formData: FormData = new FormData();
      formData.append('excelFile', this.selectedFile, this.selectedFile.name);

      this.http.post('http://localhost:8010/api/band/upload', formData).subscribe({
        next: (response) => {
            console.log(response, 'refresh');
            this.refreshBandList.emit('refresh');
          // Réinitialisation de l'input file après l'upload
          fileInput.value = '';
          this.selectedFile = undefined;
        },
        error: (error) => console.error(error)
      });
    } else {
      console.error('Aucun fichier sélectionné');
    }
  }
}
