import {Component} from '@angular/core';
import {RouterOutlet} from '@angular/router';
import {MatTableModule} from "@angular/material/table";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {CommonModule} from "@angular/common";
import {Observable} from "rxjs";
import {UploadComponent} from "./upload/upload.component";
import {MatIcon} from "@angular/material/icon";
import {MatMiniFabButton} from "@angular/material/button";
import {BandBtn, BandDialog} from "./group-dialog/band-dialog.component";

export interface Band {
    id: number;
    name: string;
    origin: string;
    city: string;
    start_year: string;
    end_year?: string;
    founders?: string;
    members: number;
    style?: string;
    description: string;
}

@Component({
    selector: 'app-root',
    standalone: true,
    imports: [
        RouterOutlet,
        HttpClientModule,
        CommonModule,
        MatTableModule,
        UploadComponent,
        MatIcon,
        MatMiniFabButton,
        BandBtn,
        BandDialog
    ],
    templateUrl: './app.component.html',
    styleUrl: './app.component.scss'
})
export class AppComponent {

    constructor(private http: HttpClient) {
        this.refreshBandList()
    }

    displayedColumns: string[] = [
        'name',
        'origin',
        'city',
        'start_year',
        'end_year',
        'founders',
        'members',
        'style',
        'description',
        'actions'
    ];
    bands$!: Observable<Band[]>;

    ngOnInit() {
    }

    public refreshBandList() {
        this.bands$ = this.getBands();
    }

    private getBands() {
        return this.http.get<Band[]>('http://localhost:8010/api/bands');
    }

    public onDeleteItem(band: Band) {
        if (confirm(`Voulez-vous vraiment supprimer le groupe ${band.name} ?`)) {
            this.http.delete(`http://localhost:8010/api/band/${band.id}/delete`).subscribe(() => {
                this.refreshBandList();
            });
        }
    }
}
