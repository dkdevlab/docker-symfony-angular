import {Component, EventEmitter, Inject, Input, Optional, Output} from '@angular/core';
import {
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogTitle,
  MatDialogContent, MatDialogActions, MatDialogClose, MatDialogRef,
} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {Band} from "../app.component";
import {MatIcon} from "@angular/material/icon";
import {JsonPipe, NgIf} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {HttpClient, HttpClientModule} from "@angular/common/http";

/**
 * @title Injecting data when opening a dialog
 */
@Component({
  selector: 'band-btn',
  template: ` <button mat-mini-fab (click)="openDialog()">
              <mat-icon>edit</mat-icon>
            </button>`,
  standalone: true,
  imports: [MatButtonModule, MatIcon, FormsModule],
})
export class BandBtn {
  constructor(public dialog: MatDialog) {}
  @Input() band: Band | undefined;
  @Output() refreshBandList = new EventEmitter<string>();

  openDialog() {
    const dialogRef = this.dialog.open(BandDialog, {
      width: '600px',
      data: {...this.band},
    });

    dialogRef.afterClosed().subscribe((shouldRefresh: boolean) => {
      if (shouldRefresh) {
        this.refreshBandList.emit('refresh');
      }
    });
  }
}

@Component({
  selector: 'band-dialog',
  templateUrl: 'band-dialog.component.html',
  standalone: true,
  imports: [MatDialogTitle, MatDialogContent, NgIf, FormsModule, MatDialogActions, MatDialogClose, HttpClientModule, JsonPipe],
})
export class BandDialog {
  constructor(
      @Inject(MAT_DIALOG_DATA) public band: Band,
      private http: HttpClient,
      private dialogRef: MatDialogRef<BandDialog>
  ) {}

  isUpdating = false;

  createOrUpdateBand() {
    this.isUpdating = true;

    this.http.put(`http://localhost:8010/api/band/${this.band.id}/update`, this.band).subscribe({
      next: (response) => {
        this.isUpdating = false;
        this.dialogRef.close(true);
      },
      error: (error) => console.error(error)
    });
  }
}
