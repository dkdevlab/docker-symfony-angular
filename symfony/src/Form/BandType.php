<?php

namespace App\Form;

use App\Entity\Band;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BandType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ['required' => true])
            ->add('origin', TextType::class, ['required' => true])
            ->add('city', TextType::class, ['required' => true])
            ->add('start_year', TextType::class, ['required' => true])
            ->add('end_year', TextType::class, ['required' => false])
            ->add('founders', TextType::class, ['required' => false])
            ->add('members', IntegerType::class, ['required' => false])
            ->add('style', TextType::class, ['required' => false])
            ->add('description', TextType::class, ['required' => true])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Band::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }
}
