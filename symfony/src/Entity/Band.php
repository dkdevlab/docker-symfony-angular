<?php

namespace App\Entity;

use App\Repository\BandRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: BandRepository::class)]
class Band
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['band:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['band:read'])]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Groups(['band:read'])]
    private ?string $origin = null;

    #[ORM\Column(length: 255)]
    #[Groups(['band:read'])]
    private ?string $city = null;

    #[ORM\Column(length: 255)]
    #[Groups(['band:read'])]
    private ?string $start_year = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['band:read'])]
    private ?string $end_year = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['band:read'])]
    private ?string $founders = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups(['band:read'])]
    private ?int $members = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['band:read'])]
    private ?string $style = null;

    #[ORM\Column(type: 'text')]
    #[Groups(['band:read'])]
    private ?string $description = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getOrigin(): ?string
    {
        return $this->origin;
    }

    public function setOrigin(string $origin): static
    {
        $this->origin = $origin;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): static
    {
        $this->city = $city;

        return $this;
    }

    public function getStartYear(): ?string
    {
        return $this->start_year;
    }

    public function setStartYear(string $start_year): static
    {
        $this->start_year = $start_year;

        return $this;
    }

    public function getEndYear(): ?string
    {
        return $this->end_year;
    }

    public function setEndYear(?string $end_year): static
    {
        $this->end_year = $end_year;

        return $this;
    }

    public function getFounders(): ?string
    {
        return $this->founders;
    }

    public function setFounders(?string $founders): static
    {
        $this->founders = $founders;

        return $this;
    }

    public function getMembers(): ?int
    {
        return $this->members;
    }

    public function setMembers(?int $members): static
    {
        $this->members = $members;

        return $this;
    }

    public function getStyle(): ?string
    {
        return $this->style;
    }

    public function setStyle(?string $style): static
    {
        $this->style = $style;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }
}
