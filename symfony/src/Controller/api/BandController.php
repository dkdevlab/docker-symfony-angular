<?php

namespace App\Controller\api;

use App\Entity\Band;
use App\Form\BandType;
use App\Repository\BandRepository;
use Doctrine\ORM\EntityManagerInterface;
use JsonException;
use PhpOffice\PhpSpreadsheet\IOFactory;
use ReflectionException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;

class BandController extends AbstractController
{

    public function __construct(private readonly EntityManagerInterface $entity_manager) {}

    #[Route('/api/bands', name: 'band_list')]
    public function index(BandRepository $band_repository, SerializerInterface $serializer): JsonResponse
    {
        $bands = $band_repository->findAll();
        $json = $serializer->serialize($bands, 'json', ['groups' => 'band:read']);

        return new JsonResponse( $json, Response::HTTP_OK, [], true);
    }

    /**
     * @throws JsonException
     */
    #[Route('/api/band/{band}/update', name: 'update_band', methods: ['PUT'])]
    public function update(Request $request, FormFactoryInterface $formFactory, Band $band): JsonResponse
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $form = $formFactory->create(BandType::class, $band);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entity_manager->flush();
            return new JsonResponse(['message' => 'Band updated successfully.'], Response::HTTP_OK);
        }

        return new JsonResponse(['message' => 'Invalid data'], Response::HTTP_BAD_REQUEST);
    }

    #[Route('/api/band/{band}/delete', name: 'delete_band', methods: ['DELETE'])]
    public function delete(Band $band): JsonResponse
    {
        $this->entity_manager->remove($band);
        $this->entity_manager->flush();

        return new JsonResponse(['message' => 'Band deleted successfully'], Response::HTTP_OK);
    }

    /**
     * @throws ReflectionException
     */
    #[Route('/api/band/upload', name: 'band_upload', methods: ['POST'])]
    public function uploadExcel(Request $request, EntityManagerInterface $entityManager, BandRepository $band_repository): Response
    {
        $file = $request->files->get('excelFile');

        if (!$file) {
            return new JsonResponse(['error' => 'No file provided'], Response::HTTP_BAD_REQUEST);
        }

        $spreadsheet = IOFactory::load($file->getPathname());
        $data = $this->getDataFromSpreadsheet($spreadsheet);

        // Récupérer les noms de tous les groupes existants
        $existingBandNames = $band_repository->createQueryBuilder('b')
            ->select('b.name')
            ->getQuery()
            ->getResult();
        $existingBandNamesArray = array_column($existingBandNames, 'name');


        foreach ($data as $row) {
            // Éviter les doublons
            if (in_array($row['Nom du groupe'], $existingBandNamesArray, true)) {
                continue;
            }
            $band = new Band();
            $band->setName($row['Nom du groupe']);
            $band->setOrigin($row['Origine']);
            $band->setCity($row['Ville']);
            $band->setStartYear($row['Année début']);
            $band->setEndYear($row['Année séparation']);
            $band->setFounders($row['Fondateurs']);
            $band->setMembers($row['Membres']);
            $band->setStyle($row['Courant musical']);
            $band->setDescription($row['Présentation']);

            $entityManager->persist($band);
        }

        $entityManager->flush();

        return new JsonResponse(['message' => 'Fichier traité avec succès'], Response::HTTP_OK);
    }

    private function getDataFromSpreadsheet($spreadsheet): array
    {
        $data = [];
        $isHeader = true; // Un indicateur pour suivre si la ligne est un en-tête
        $headers = []; // Un tableau pour stocker les en-têtes

        foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
            foreach ($worksheet->getRowIterator() as $row) {
                $rowIndex = $row->getRowIndex();

                // Traitement des en-têtes séparément
                if ($isHeader) {
                    foreach ($row->getCellIterator() as $cell) {
                        $headers[] = $cell->getCalculatedValue();
                    }
                    $isHeader = false; // Ne pas relire les en-têtes
                    continue;
                }

                $rowData = [];
                foreach ($row->getCellIterator() as $cell) {
                    $rowData[] = $cell->getCalculatedValue();
                }
                $data[] = array_combine($headers, $rowData);
            }
            // Réinitialisez $isHeader pour le traitement de la prochaine feuille, si nécessaire
            $isHeader = true;
        }

        return $data;
    }

}
