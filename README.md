# Projet Symfony-Angular d'Import XLS

Ce projet combine Symfony pour l'API backend et Angular pour le frontend, offrant une solution complète pour l'importation de données à partir de fichiers XLS.

## Configuration initiale

Pour commencer à travailler avec le projet, suivez les étapes de configuration ci-dessous :

### Pré-requis

- Docker et Docker Compose installés sur votre machine.
- Node.js et npm pour Angular (si des modifications ou des tests hors conteneur Docker sont nécessaires).

### Configuration de l'Environnement

1. **Configuration des variables d'environnement** :

   Copiez et renommez le fichier `.env.example` en `.env` pour initialiser votre configuration d'environnement :

    ```sh
    cp .env.example .env
    ```

2. **Personnalisation du port Angular** :

   Si vous souhaitez changer le port par défaut sur lequel l'application Angular s'exécute, localisez la variable `ANGULAR_PORT` dans votre fichier `.env` et ajustez-la selon votre préférence :

    ```env
    ANGULAR_PORT=8000
    ```

   Remplacez `8000` par le port de votre choix.

3. **Installation des dépendances JavaScript** :

   Avant de démarrer les conteneurs Docker, naviguez vers le dossier Angular et installez les dépendances nécessaires :

    ```sh
    cd angular
    npm install
    ```

   Ceci installe toutes les dépendances listées dans le fichier `package.json` de votre projet Angular.

### Démarrage du Projet avec Docker

1. **Lancement des conteneurs Docker** :

   Dans le répertoire racine du projet, exécutez la commande suivante pour démarrer l'application Angular et l'API Symfony en utilisant Docker :

    ```sh
    docker compose up -d
    ```

   L'option `-d` lance Docker Compose en mode détaché, libérant votre terminal après le démarrage des conteneurs.

2. **Installation des dépendances Symfony** :

   Après le démarrage des conteneurs, installez les dépendances Symfony en exécutant `composer install` à l'intérieur du conteneur Symfony :

    ```sh
    docker compose exec symfony bash -c "composer install"
    ```

3. **Jouer les migrations Symfony** :

   La base de données 'symfony' étant créée par le docker mysql, il faut jouer les migrations en exécutant `bin/console d:m:m` à l'intérieur du conteneur Symfony :

    ```sh
    docker compose exec symfony bash -c "bin/console d:m:m"
    ```

   Cette commande lance une session shell à l'intérieur du conteneur `symfony` où `bin/console d:m:m` (syntaxe raccourcie de `bin/console doctrine:migration:migrate`) est exécuté pour créer les tables nécessaires dans la base de données.

4. **Accès aux applications** :

   Une fois les conteneurs démarrés et les dépendances installées, vous pouvez accéder à l'application Angular à l'adresse [http://localhost:ANGULAR_PORT](http://localhost:ANGULAR_PORT) (remplacez `ANGULAR_PORT` par le port que vous avez spécifié).
